package gemwars.tools;
import java.util.ArrayList;
import java.util.Random;
public class Board {
	ArrayList<Box> tab;
	Random rnd;
	
	public Board(){
		rnd = new Random();
	}
	
	public ArrayList<Box> crearTablero(){
		tab = new ArrayList<>();
		for (int i=0; i<20; i++){
			Box cas = new Box();
			cas.setStone(new ArrayList<>());
			tab.add(cas);
		}
		generarPiedrasTablero();
		System.out.println("Tablero creado");
		return tab;
	}
	
	// generar n piedras de diversos tipos
	public void generarPiedrasTablero(){
		String[] gems =  {"Diamond","Ruby","Sapphire","Emerald","Onyx"};
		for (int i=0; i<gems.length; i++){						
			int nJewels = 0;
			do{
				nJewels = rnd.nextInt(10);			
			}
			while (nJewels < 4 );	
			System.out.println("Numero de joyas de " + gems[i]+ "  :  " + nJewels);
			for (int j=0; j< nJewels; j++){
				asignarPiedrasTablero(gems[i]);
			}
		}
	}
	
	public void asignarPiedrasTablero(String gema){
		ArrayList<String> temp;
		boolean ciclo = true;
		while (ciclo){
			int pos = rnd.nextInt(19);
			if (tab.get(pos).getStone().size() < 3){
				temp = tab.get(pos).getStone();
				temp.add(gema);
				tab.get(pos).setStone(temp);
				ciclo = false;
			}
		}			
	}
}