package gemwars.app;

import gemwars.app.Simulador;

public class Main {

	public static void main(String[] args) {
		Main.run();
	}
	
	public static void run(){
		Simulador sim = new Simulador();
		sim.initWarrior();
		sim.crearTablero();
		System.out.println();
		System.out.println("Las joyas quedaron distribuidas  de la siguiente manera");
		sim.verTablero();
		sim.letsPlay();
	}
}
