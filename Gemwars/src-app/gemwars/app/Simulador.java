package gemwars.app;
import gemwars.decorator.*;
import gemwars.gamer.Gamer;
import gemwars.gamer.Warrior;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;
import gemwars.tools.Board;
import gemwars.tools.Box;

public class Simulador{
	public int dado;
	public Random rnd; 
	ArrayList<Box> tab;
	ArrayList<Gamer> warriors;
	
	public Simulador (){
		rnd = new Random();
	}
	
	public void initWarrior(){
		warriors = new ArrayList<>();
		for (int i=0; i<3; i++){
			Gamer jugador = new Warrior();
			jugador.setNombre("Jugador " + (i + 1));
			//jugador.setPuntos(4);
			jugador.setStatus("activo");
			jugador.setPosicion(-1);
			warriors.add(jugador);
		}
	}
	
	public void crearTablero(){
		Board tablero = new Board();
		tab = tablero.crearTablero();
	}
 
	public int lanzarDado(){
		dado = rnd.nextInt(3) + 1;
		return dado;
	}
	
	//Jugador 1 es el jugador actual en curso, el jugador2 es el contrincante que esta en la posicion del tablero
	public void combatir(Gamer jugador1, Gamer jugador2){
		int dif=0;
		
		if (jugador1.getStatus() != "KO" && jugador2.getStatus() != "KO" && (jugador1.getPoints() != jugador2.getPoints())){
			System.out.println("Se inicia el combate");
			System.out.println("El " + jugador1.getNombre() + " esta luchando contra el  "+ jugador2.getNombre());
			if (jugador1.getPoints() > jugador2.getPoints()){
				dif = jugador1.getPoints() - jugador2.getPoints();
				jugador2 = new Weakness(jugador2, dif);
				//jugador2.setPuntos(jugador2.getPoints() - dif);
				
				System.out.println("El " + jugador1.getNombre() + " ha ganando el combate");
				// Aqui se invoca al decorador
				if (jugador2.getPoints()<= 0){
					jugador2.setStatus("KO");
				}
			}			
			else{
				dif = jugador2.getPoints() - jugador1.getPoints();					
				//jugador1.setPuntos(jugador1.getPoints() - dif);
				jugador1 = new Weakness(jugador1, dif);
				System.out.println("El " + jugador2.getNombre() + " ha ganando el combate");
				//Aqui se invoca al decorador
				if (jugador1.getPoints()<= 0){
					jugador1.setStatus("KO");
				}
			}
		}
		else{
			System.out.println("No hubo combate.");
		}
	}
	
	public Gamer absorberPiedra(Gamer jugador, int pos, ArrayList<String> box){
//		Se aplica el patron decarador
		if (box.size() > 0){
			switch (box.get(0)){
				case "Diamond":	
					jugador = new Diamond(jugador);
					break;
				case "Ruby":
					jugador = new Ruby(jugador);
					break;
				case "Sapphire":
					jugador = new Sapphire(jugador);
					break;
				case "Emerald":
					jugador = new Emerald(jugador);
					break;			 
				case "Onyx":
					jugador = new Onyx(jugador);
					break;
			}
			System.out.println("Se ha absorbido el poder de la gema " + box.get(0));
			box.remove(0);
			System.out.println("El " + jugador.getNombre() + " esta aumentando poder a " + jugador.getPoints() + " puntos");
		}	
		return jugador;
	}
	
	//Mostrar las piedras en el tablero
	public void verTablero(){
		int i = 1;
		for (Box cas : tab){
			 System.out.println(i + "   "  + cas.getStone());
			 i++;
		}
	}
	
	//Metodo que lleva el control del turno de un jugador
	public void letsPlay(){		
		int terminados;
		int dado= 0;
		boolean finalizar = false;
		int turno = 0;
		do{
			try{
							
				Gamer warriorOnTurn = warriors.get(turno);
				System.out.println();
				if ( !warriorOnTurn.getStatus().equals("KO") && !warriorOnTurn.getStatus().equals("Esperando")){
					dado = lanzarDado();
					System.out.println("El " +  warriorOnTurn.getNombre() + " con " +  warriorOnTurn.getPoints() + " puntos en la posicion " +
					( warriorOnTurn.getPosicion() + 1 ) + " ha lanzado el dado y ha caido el numero " + dado);
					turn(warriorOnTurn, dado, turno);
				}
				
				if (turno == warriors.size() - 1){
					turno = 0;
					
				}
				else{
					turno++;
				}
				
				terminados = 0;
				
				for (Gamer warrior : warriors){
					if (warrior.getStatus().equals("KO") || warrior.getStatus().equals("Esperando")){
						terminados++;
					}
				}
				if (terminados == warriors.size()){
					System.out.println("Se ha terminado el juego");
					finalizar = true;
				}
				Thread.sleep(1000);
			}
			catch(InterruptedException e){
				e.printStackTrace();
			}
		}
		while (!finalizar);
		
		for (Gamer warrior : warriors){
			System.out.println(warrior.getNombre()+" - "+ ( warrior.getPosicion() + 1 )+" - "+ warrior.getPoints()+" - "+ warrior.getStatus());
		}
		
		winner();
	}
	
	//Metodo que verifica la posicion, si absorbe piedra o si combate
	public void turn(Gamer warrior, int dado, int turno){
		
		int posicion = warrior.getPosicion() + dado;

		if (posicion >=  19){
//			Ha llegado al final del tablero
			warrior.setPosicion(19);
			warrior.setStatus("Esperando");
		}
		else{
			warrior.setPosicion(posicion);
			System.out.println("Se movio el jugador a la posicion: "+ ( warrior.getPosicion() + 1) );
			
			if (!tab.get(posicion).getStone().equals("")){
				warriors.set(turno, absorberPiedra(warrior, posicion, tab.get(posicion).getStone())); //jugador, posicion, casilla
				
			}
			
			for (Gamer enemy : warriors){
				//Aqui se realiza una busqueda de jugadores en la posicion actual
				if (!warrior.getNombre().equals(enemy.getNombre()) && enemy.getPosicion() == warrior.getPosicion()  ){
						combatir(warrior, enemy);
				}						
			}			
		}				
	}
	
	public void winner() {
		warriors.sort(Ordenar);
		System.out.println("El ganador es : " + warriors.get(warriors.size()-1).getNombre() + "  con  "  + warriors.get(warriors.size()-1).getPoints() + "  Puntos");
	}
	
	public Comparator<Gamer> Ordenar = new Comparator<Gamer>() {
        @Override
        public int compare(Gamer j1, Gamer j2) {
            Integer cost1 = j1.getPoints();
            Integer cost2 = j2.getPoints();
            return cost1.compareTo(cost2);
        }
    };
}
