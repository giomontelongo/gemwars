package gemwars.gamer;

public abstract class Gamer{

	String description = "Gamer";
	
	public String getDescription(){
		return description;
	}
	
	public abstract int getPoints();

	String nombre;
	int posicion;
	String status;
	
	public abstract String getNombre();
	public abstract int getPosicion();
	public abstract String getStatus();
	
	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	public abstract void setPosicion(int posicion);
	public abstract void setStatus(String status);
}