package gemwars.gamer;


public class Warrior extends Gamer{
	public Warrior(){
		description = "Gamer Warrior";
	}
	
	@Override
	public int getPoints() {
		return 4;
	}
	
	@Override
	public String getNombre() {
		return nombre;
	}
	
	@Override
	public int getPosicion() {
		return posicion;
	}
	
	@Override
	public String getStatus() {
		return status;
	}
 
	@Override
	public void setPosicion(int posicion) {
		this.posicion = posicion;
	}
	
	@Override
	public void setStatus(String status) {
		this.status = status;
	}
}