package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import gemwars.decorator.Onyx;
import gemwars.gamer.Gamer;
import gemwars.gamer.Warrior;

public class WarriorWithOnyx {

	Gamer warrior;

	@Before
	public void setUp() throws Exception {
		warrior = new Warrior();
		warrior.setNombre("Giovanni");
		warrior.setPosicion(1);
		warrior.setStatus("KO");
		warrior = new Onyx(warrior);
	}

	@Test
	public void testGetDescription() {
		assertTrue(warrior.getDescription().equals("Gamer Warrior, Onyx Stone"));
	}

	@Test
	public void testGetPoints() {
		assertTrue(warrior.getPoints() == 5);
	}

	@Test
	public void testGetNombre() {
		assertTrue(warrior.getNombre() == "Giovanni");
	}

	@Test
	public void testGetPosicion() {
		assertTrue(warrior.getPosicion() == 1);
	}

	@Test
	public void testGetStatus() {
		assertTrue(warrior.getStatus() == "KO");
	}
}
