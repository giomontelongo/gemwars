package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import gemwars.decorator.Sapphire;
import gemwars.gamer.Gamer;
import gemwars.gamer.Warrior;

public class WarriorWithSapphire {

	Gamer warrior;

	@Before
	public void setUp() throws Exception {
		warrior = new Warrior();
		warrior.setNombre("Aldo");
		warrior.setPosicion(1);
		warrior.setStatus("KO");
		warrior = new Sapphire(warrior);
	}

	@Test
	public void testGetDescription() {
		assertTrue(warrior.getDescription().equals("Gamer Warrior, Sapphire Stone"));
	}

	@Test
	public void testGetPoints() {
		assertTrue(warrior.getPoints() == 7);
	}

	@Test
	public void testGetNombre() {
		assertTrue(warrior.getNombre() == "Aldo");
	}

	@Test
	public void testGetPosicion() {
		assertTrue(warrior.getPosicion() == 1);
	}

	@Test
	public void testGetStatus() {
		assertTrue(warrior.getStatus() == "KO");
	}
}
