package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import gemwars.decorator.Weakness;
import gemwars.gamer.Gamer;
import gemwars.gamer.Warrior;

public class WarriorWithWeakness {

	Gamer warrior;
	
	@Before
	public void setUp() throws Exception {
		
		warrior = new Warrior();
		warrior.setNombre("Aldo");
		warrior.setPosicion(2);
		warrior.setStatus("KO");
		warrior = new Weakness(warrior, 3);
	}

	@Test
	public void testGetDescription() {
		assertTrue(warrior.getDescription().equals("Gamer Warrior, Weakness Stone"));
	}

	@Test
	public void testGetPoints() {
		assertTrue(warrior.getPoints() == 1);
	}

	@Test
	public void testGetNombre() {
		assertTrue(warrior.getNombre() == "Aldo");
	}

	@Test
	public void testGetPosicion() {
		assertTrue(warrior.getPosicion() == 2);
	}

	@Test
	public void testGetStatus() {
		assertTrue(warrior.getStatus() == "KO");
	}
}
