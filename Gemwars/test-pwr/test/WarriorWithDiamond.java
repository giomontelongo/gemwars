package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import gemwars.decorator.Diamond;
import gemwars.gamer.Gamer;
import gemwars.gamer.Warrior;

public class WarriorWithDiamond {
	
	Gamer warrior;

	@Before
	public void setUp() throws Exception {
		warrior = new Warrior();
		warrior.setNombre("Giovanni");
		warrior.setPosicion(1);
		warrior.setStatus("Esperando");
		warrior = new Diamond(warrior);
	}

	@Test
	public void testGetDescription() {
		assertTrue(warrior.getDescription().equals("Gamer Warrior, Diamond Stone"));
	}

	@Test
	public void testGetPoints() {
		assertTrue(warrior.getPoints() == 12);
	}

	@Test
	public void testGetNombre() {
		assertTrue(warrior.getNombre() == "Giovanni");
	}

	@Test
	public void testGetPosicion() {
		assertTrue(warrior.getPosicion() == 1);
	}

	@Test
	public void testGetStatus() {
		assertTrue(warrior.getStatus() == "Esperando");
	}
}
