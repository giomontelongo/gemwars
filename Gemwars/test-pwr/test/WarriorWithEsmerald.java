package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import gemwars.decorator.Emerald;
import gemwars.gamer.Gamer;
import gemwars.gamer.Warrior;

public class WarriorWithEsmerald {

	Gamer warrior;

	@Before
	public void setUp() throws Exception {
		warrior = new Warrior();
		warrior.setNombre("Aldo");
		warrior.setPosicion(1);
		warrior.setStatus("KO");
		warrior = new Emerald(warrior);
	}

	@Test
	public void testGetDescription() {
		assertTrue(warrior.getDescription().equals("Gamer Warrior, Esmerald Stone"));
	}
	
	@Test
	public void testGetPoints() {
		assertTrue(warrior.getPoints() == 6);
	}

	@Test
	public void testGetNombre() {
		assertTrue(warrior.getNombre() == "Aldo");
	}

	@Test
	public void testGetPosicion() {
		assertTrue(warrior.getPosicion() == 1);
	}

	@Test
	public void testGetStatus() {
		assertTrue(warrior.getStatus() == "KO");
	}
}
