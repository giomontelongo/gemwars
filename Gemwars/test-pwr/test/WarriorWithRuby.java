package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import gemwars.decorator.Ruby;
import gemwars.gamer.Gamer;
import gemwars.gamer.Warrior;

public class WarriorWithRuby {
	
	Gamer warrior;

	@Before
	public void setUp() throws Exception {
		warrior = new Warrior();
		warrior.setNombre("Giovanni");
		warrior.setPosicion(1);
		warrior.setStatus("Esperando");
		warrior = new Ruby(warrior);
	}

	@Test
	public void testGetDescription() {
		assertTrue(warrior.getDescription().equals("Gamer Warrior, Ruby Stone"));
	}

	@Test
	public void testGetPoints() {
		assertTrue(warrior.getPoints() == 9);
	}

	@Test
	public void testGetNombre() {
		assertTrue(warrior.getNombre() == "Giovanni");
	}

	@Test
	public void testGetPosicion() {
		assertTrue(warrior.getPosicion() == 1);
	}

	@Test
	public void testGetStatus() {
		assertTrue(warrior.getStatus() == "Esperando");
	}
}
