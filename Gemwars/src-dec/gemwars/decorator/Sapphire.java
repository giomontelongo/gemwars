package gemwars.decorator;

import gemwars.gamer.Gamer;

public class Sapphire extends GemDecorator {
	
	Gamer gamer;
	
	public Sapphire(Gamer gamer){
		this.gamer = gamer;
	}

	@Override
	public String getDescription() {
		return gamer.getDescription() + ", Sapphire Stone";
	}

	@Override
	public int getPoints() {
		return 3 + gamer.getPoints();
	}
	
	@Override
	public String getNombre() {
		return gamer.getNombre();
	}

	@Override
	public int getPosicion() {
		return gamer.getPosicion();
	}

	@Override
	public String getStatus() {
		return gamer.getStatus();
	}

	
	@Override
	public void setPosicion(int posicion) {
		gamer.setPosicion(posicion);
		
	}

	@Override
	public void setStatus(String status) {
		gamer.setStatus(status);
		
	}
}
