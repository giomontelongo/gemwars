package gemwars.decorator;

import gemwars.gamer.Gamer;

public class Onyx extends GemDecorator{
	
	Gamer gamer;
	
	public Onyx(Gamer gamer){
		this.gamer = gamer;
	}

	@Override
	public String getDescription() {
		return gamer.getDescription() + ", Onyx Stone";
	}

	@Override
	public int getPoints() {
		return 1 + gamer.getPoints();
	}
	@Override
	public String getNombre() {
		return gamer.getNombre();
	}

	@Override
	public int getPosicion() {
		return gamer.getPosicion();
	}

	@Override
	public String getStatus() {
		return gamer.getStatus();
	}

	
	@Override
	public void setPosicion(int posicion) {
		gamer.setPosicion(posicion);
		
	}

	@Override
	public void setStatus(String status) {
		gamer.setStatus(status);
		
	}
}