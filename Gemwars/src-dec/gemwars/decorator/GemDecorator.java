package gemwars.decorator;

import gemwars.gamer.Gamer;

public abstract class GemDecorator extends Gamer {
	public abstract String getDescription();
}
