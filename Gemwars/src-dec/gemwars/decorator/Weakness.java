package gemwars.decorator;

import gemwars.gamer.Gamer;

public class Weakness extends GemDecorator {
	
	Gamer gamer;
	private int points;
	
	public Weakness(Gamer gamer, int points){
		this.gamer = gamer;
		this.points = points;
	}

	@Override
	public String getDescription() {
		return gamer.getDescription() + ", Weakness Stone";
	}

	@Override
	public int getPoints() {
		// TODO Auto-generated method stub
		return ( -1 * points ) + gamer.getPoints() ;
	}

	@Override
	public String getNombre() {
		return gamer.getNombre();
	}

	@Override
	public int getPosicion() {
		return gamer.getPosicion();
	}

	@Override
	public String getStatus() {
		return gamer.getStatus();
	}

	
	@Override
	public void setPosicion(int posicion) {
		gamer.setPosicion(posicion);
		
	}

	@Override
	public void setStatus(String status) {
		gamer.setStatus(status);
		
	}
}