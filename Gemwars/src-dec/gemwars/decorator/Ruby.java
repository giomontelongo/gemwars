package gemwars.decorator;

import gemwars.gamer.Gamer;

public class Ruby extends GemDecorator {
	
	Gamer gamer;
	
	public Ruby(Gamer gamer){
		this.gamer = gamer;
	}

	@Override
	public String getDescription() {
		return gamer.getDescription() + ", Ruby Stone";
	}

	@Override
	public int getPoints() {
		return 5 + gamer.getPoints();
	}
	@Override
	public String getNombre() {
		return gamer.getNombre();
	}

	@Override
	public int getPosicion() {
		return gamer.getPosicion();
	}

	@Override
	public String getStatus() {
		return gamer.getStatus();
	}

	
	@Override
	public void setPosicion(int posicion) {
		gamer.setPosicion(posicion);
		
	}

	@Override
	public void setStatus(String status) {
		gamer.setStatus(status);
		
	}
}